## Here I collect different testsets of problems

* **CBLIB.test**:
> contains all instances from the CBLib (convex benchmark library)

* **convexprojection.test**:
> contains all instances from MINLPLib2 for which the convex relaxation has at least *two* convex constraints [after SCIP reformulation]

* **gauge.test**:
> contains all instances from MINLPLib2 for which there is at least one convex quadratic constraint [after SCIP reformulation]

* **MittelmanMISOCP.test**:
> mixed integer instances from CBLIB and http://www.ieor.berkeley.edu/~atamturk/data/assortment.optimization/models/100_100_hard/100_100_hard_conic/
> used by H. Mittelman in a benchmark http://plato.asu.edu/ftp/socp.html

* **MittelmanSOCP.test**:
> continuous instances from CBLIB used by H. Mittelman in a benchmark: http://plato.asu.edu/ftp/socp.html

* **convexMINLIB2.test**:
> contains all instances from MINLPLib2 for which continuous relaxation has proven to be convex

* **convexQP_MINLIB2.test**:
> contains all quadratic (cons and/or obj) instances from MINLPLib2 for which continuous relaxation has proven to be convex

**Note** MINLPlib2 changes from time to time, so testset which are a subset of MINLPLib2 might be outdated
